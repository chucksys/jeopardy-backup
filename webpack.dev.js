const config = require('./webpack.config.js');
const { merge } = require('webpack-merge');

module.exports = merge(config, {
  mode: 'development',
  devServer: {
    port: 8080,
    host: 'localhost',
    historyApiFallback: true,
    contentBase: './public',
    watchContentBase: true,
    compress: true,
    publicPath: '/js/',
    open: true
  }
});
