import Vue, { PropType } from 'vue';
import { Question } from '../question';

export default Vue.extend({
  props: {
    teams: Array as PropType<string[]>,
    question: Object as PropType<Question>,
  },
  data() {
    return {
      showAnswer: false,
    };
  },
  methods: {
    onShowAnswer() {
      this.showAnswer = true;
      this.question.seenAnswer = true;
    },
  },
  template: `
<div class="card blue large">
  <div class="card-content white-text">
    <span class="card-title">{{ question.points }} points</span>
    <p v-html="question.hint"></p>
    <p v-if="showAnswer" v-html="question.answer"></p>
  </div>
  <div class="card-action">
    <a href="#" v-if="question.seenAnswer" v-for="team in teams" @click="$emit('answer-question', team, question)">{{ team }}</a>
    <a href="#" v-if="!question.seenAnswer" @click="onShowAnswer">Show Answer</a>
    <a href="#" @click="$emit('cancel-focus')">Close</a>
  </div>
</div>
`,
});
