import Vue from 'vue';
import CategoryQuestionList from './category-question-list';
import QuestionCard from './question-card';
import { Question } from '../question';

interface FocusedQuestion {
  clicked: boolean,
  question?: Question,
}

function makeUnfocused(): FocusedQuestion {
  return {
    clicked: false,
  };
}

export default Vue.extend({
  props: {
    questions: Object,
    scores: Object,
  },
  data() {
    return {
      focused: makeUnfocused(),
    };
  },
  methods: {
    onClickQuestion(q: Question) {
      if (q.seenAnswer) {
        return;
      }

      this.focused = { clicked: true, question: q };
    },
    onCancelFocus() {
      this.focused = makeUnfocused();
    },
    onAnswerQuestion(team: string, q: Question) {
      if (q.answeredBy === '') {
        this.scores[team] += q.points;
        q.answeredBy = team;
      }
    },
  },
  template: `
<div class="question-panel">
  <div class="category center-align" v-if="!focused.clicked" v-for="category in Object.keys(questions)" :key="category">
    <span class="header blue white-text z-depth-1">{{ category }}</span>
    <category-question-list :questions="questions[category]" @click-question="onClickQuestion"></category-question-list>
  </div>
  <question-card v-if="focused.clicked" :teams="Object.keys(scores)" :question="focused.question" @answer-question="onAnswerQuestion" @cancel-focus="onCancelFocus"></question-card>
</div>`,
  components: {
    'category-question-list': CategoryQuestionList,
    'question-card': QuestionCard,
  },
});
