import Vue from 'vue';
import { Question } from '../question';

export default Vue.extend({
  props: {
    questions: Array,
  },
  methods: {
    questionClass(q: Question) {
      return {
        blue: !q.seenAnswer,
        grey: q.seenAnswer,
        unanswered: !q.seenAnswer,
        answered: q.seenAnswer,
      };
    },
  },
  template: `
<ul class="question-list">
  <li class="question white-text z-depth-1"
  v-for="q in questions"
  :class="questionClass(q)"
  :key="q.hint"
  @click="$emit('click-question', q)">{{ q.points }}</li>
</ul>
`,
});
