import Vue from 'vue';

export default Vue.extend({
  props: {
    scores: Object,
  },
  data() {
    return {
      addTeamPressed: false,
      teamName: '',
    };
  },
  methods: {
    onAddTeam() {
      if (Object.prototype.hasOwnProperty.call(this.scores, this.teamName)) {
        return;
      }

      this.$set(this.scores, this.teamName, 0);
      this.addTeamPressed = false;
      this.teamName = '';
    },
  },
  template: `
<li class="add-team row">
  <a v-if="!addTeamPressed" @click="addTeamPressed = true" class="waves-effect waves-light btn">Add team<i class="material-icons">add</i></a>
  <span v-if="addTeamPressed">
    <input v-model="teamName" type="text" class="white-text col s10" placeholder="Team name">
    <a @click="onAddTeam" class="col s2 waves-effect waves-light btn"><i class="material-icons">add</i></a>
  </span>
</li>
`,
});
