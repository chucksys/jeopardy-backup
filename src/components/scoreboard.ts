import Vue from 'vue';
import AddTeam from './add-team';

interface ScoreboardData {
  focused: Set<string>,
}

export default Vue.extend({
  props: {
    scores: Object,
  },
  data(): ScoreboardData {
    return {
      focused: new Set(),
    };
  },
  methods: {
    onRemove(name: string) {
      const clickedOnce = this.focused.has(name);
      const bt = document.getElementById(name) as HTMLElement;
      if (clickedOnce) {
        this.$delete(this.scores, name);
        this.focused.delete(name);
        bt.classList.remove('red');
      } else {
        this.focused.add(name);
        bt.classList.add('red');

        // Reset everything after a delay
        setTimeout(() => {
          this.focused.delete(name);
          bt.classList.remove('red');
        }, 1000);
      }
    },
  },
  components: {
    'add-team': AddTeam,
  },
  template: `
<ul class="sidenav sidenav-fixed blue">
  <li class="center-align white-text"><h3>Scoreboard</h3></li>
  <li class="blue lighten-3 team" v-for="teamName in Object.keys(scores)">
    <button :id="teamName" class="waves-effect waves-light btn-floating" @click="onRemove(teamName)">
      <i class="material-icons">remove_circle</i>
    </button>
    <span class="name">{{ teamName }}</span>
    <span class="badge">{{ scores[teamName] }}</span>
  </li>

  <add-team :scores="scores"></add-team>
</ul>`,
});
