import Vue from 'vue';
import Scoreboard from './components/scoreboard';
import QuestionPanel from './components/question-panel';
import { QuestionType } from './question';

const app = new Vue({
  el: '#app',
  data: {
    scores: {'Team Hulu': 100, 'Team Bond': 300},
    questions: {
      'Psalm': [
        {
          type: QuestionType.Open,
          hint: 'List 3 of the chapters we have studied this year.',
          points: 200,
          category: 'Psalm',
          answer: '1, 8, 23, 51, 90, 139, 16:1-11, 19:1-14, 121, 119:1-16, 119:89-106',
          answeredBy: '',
          seenAnswer: false,
        },
        {
          type: QuestionType.MC,
          hint: '1 + 1 = ?',
          points: 300,
          category: 'Psalm',
          answer: '2',
          answeredBy: '',
          seenAnswer: false,
        },
      ],
      '4 Loves': [
        {
          type: QuestionType.Open,
          hint: 'List the names of the 4 loves.',
          points: 200,
          category: '4 Loves',
          answer: 'Agape, Eros, Philia, Storge',
          answeredBy: '',
          seenAnswer: false,
        },
      ],
    },
  },
  components: {
    'scoreboard': Scoreboard,
    'question-panel': QuestionPanel,
  },
});
