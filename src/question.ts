export enum QuestionType {
  Open, MC
};

export interface Question {
  type: QuestionType,
  hint: string,
  points: number,
  category: string,
  answer: string,
  answeredBy: string,
  seenAnswer: boolean,
};
